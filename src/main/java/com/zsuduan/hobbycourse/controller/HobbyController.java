package com.zsuduan.hobbycourse.controller;

import com.zsuduan.hobbycourse.bean.HobbyBean;
import com.zsuduan.hobbycourse.service.HobbyService;
import com.zsuduan.hobbycourse.util.WebSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class HobbyController{

    @Autowired
    HobbyService hobbyService;

    @RequestMapping("/addHobby")
    public String show(){
        return "hobbyInfo";
    }

    //添加个人兴趣
    @RequestMapping("/addUserHobby")
    public String addHobby(String name, String summary, String others, HttpSession session, Model model) {
        int uid = (Integer) session.getAttribute(WebSecurityConfig.SESSION_KEY);
        int result = hobbyService.addHobby(uid,name,summary,others);
        if (result==0){
            model.addAttribute("reason","增加个人兴趣成功");
            return "redirect:/userInfo";
        }else {
            model.addAttribute("reason","增加个人兴趣失败");
            return "redirect:/userInfo";
        }
    }

    //显示个人兴趣
//    @RequestMapping("/getUserHobby")
//    public String getUserHobby(HttpSession session, Model model){
//        int uid = (Integer) session.getAttribute(WebSecurityConfig.SESSION_KEY);
//        HobbyBean hobbyBean = hobbyService.getUserHobby(uid);
//        model.addAttribute("hobby",hobbyBean);
//        return "userInfo";
//
//    }

    @RequestMapping("/modifyUserHobbyPage")
    public String modifyUserHobbyPage(int id, Model model) {
        HobbyBean hobby = hobbyService.getUserHobbyById(id);
        model.addAttribute("hobby",hobby);
        return "hobbyInfo";
    }

    //修改个人兴趣
    @RequestMapping("/modifyUserHobby")
    public String modifyUserHobby(int id,String name,String summary,String others,HttpSession session,Model model){
        HobbyBean hobby = hobbyService.getUserHobbyById(id);
        int result = hobbyService.modifyUserHobby(id,name,summary,others);
        if (result==0){
            model.addAttribute("reason","修改成功");
            return "redirect:/userInfo";
        }else {
            model.addAttribute("reason","修改失败");
            return "redirect:/userInfo";
        }
    }

    //删除个人兴趣
    @RequestMapping("/deleteUserHobby")
    public String deleteUserHobby(int id,Model model){
        int result = hobbyService.deleteUserHobby(id);
        if (result==0){
            model.addAttribute("reason","修改成功");
            return "redirect:/userInfo";
        }else {
            model.addAttribute("reason","修改失败");
            return "redirect:/userInfo";
        }
    }

}
