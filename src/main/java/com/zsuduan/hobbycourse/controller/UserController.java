package com.zsuduan.hobbycourse.controller;

import com.zsuduan.hobbycourse.bean.HobbyBean;
import com.zsuduan.hobbycourse.bean.UserBean;
import com.zsuduan.hobbycourse.service.HobbyService;
import com.zsuduan.hobbycourse.service.UserService;
import com.zsuduan.hobbycourse.util.WebSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class UserController {
    //将Service注入Web层
    @Autowired
    UserService userService;
    @Autowired
    HobbyService hobbyService;

    @RequestMapping("modifyInfo")
    public String modifyInfo(HttpSession session,Model model){
        int id = (Integer) session.getAttribute(WebSecurityConfig.SESSION_KEY);
        UserBean userBean=userService.getUserInfo(id);
        model.addAttribute("user",userBean);
        return "modifyInfo";
    }

    //获取用户信息
    @RequestMapping(value = "/userInfo")
    public String getUserInfo(HttpSession session,Model model) {
        int id = (Integer) session.getAttribute(WebSecurityConfig.SESSION_KEY);
        UserBean userBean=userService.getUserInfo(id); //显示个人信息
        model.addAttribute("user",userBean);
        List<HobbyBean> hobbyBean=hobbyService.getUserHobby(id); //显示个人兴趣
        model.addAttribute("hobbyList",hobbyBean);
        return "userInfo";
    }

    //修改用户信息
    @RequestMapping("/modifyUserInfo")
    public String modifyUserInfo(String name,String gender,String tel,String address,HttpSession session,Model model){
        int id = (Integer) session.getAttribute(WebSecurityConfig.SESSION_KEY);
        UserBean userBean=userService.getUserInfo(id);
        model.addAttribute("user",userBean);
        int result= userService.modifyUserInfo(id,name,gender,tel,address);
         if(result == 0){
            model.addAttribute("reason","修改信息成功");
            return "redirect:/userInfo";
        }else{
            model.addAttribute("reason","修改信息失败");
            return "redirect:/userInfo";
        }
    }





}