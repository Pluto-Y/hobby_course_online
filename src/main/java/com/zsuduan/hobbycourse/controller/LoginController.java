package com.zsuduan.hobbycourse.controller;

import com.zsuduan.hobbycourse.bean.UserBean;
import com.zsuduan.hobbycourse.service.UserService;
import com.zsuduan.hobbycourse.util.WebSecurityConfig;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@Validated
public class LoginController {
    //将Service注入Web层
    @Autowired
    UserService userService;

    @RequestMapping("/login")
    public String show(){
        return "login";
    }

    //登录
    @RequestMapping(value = "/loginIn",method = RequestMethod.POST)
    public String login(String account,
                        String password,
                        Model model,
                        HttpSession session){
        UserBean userBean=userService.loginIn(account, password);
        if (userBean!=null){
            session.setAttribute(WebSecurityConfig.SESSION_KEY, userBean.getId());
            model.addAttribute("reason","登录成功");
            return "redirect:/userInfo";
        }else {
            model.addAttribute("reason","登录失败");
            return "error";
        }
    }

    //登出
    @RequestMapping("/logout")
    public String logout(HttpSession session){
        session.removeAttribute(WebSecurityConfig.SESSION_KEY);
        return "login";
    }

    //修改密码
    @RequestMapping("/modify")
    public String modify(){
        return "modify";
    }

    @RequestMapping(value = "/modifyPassword",method = RequestMethod.POST)
    public String modifyPassword( String password1, String password2,HttpSession session, Model model) {
        int id = (Integer) session.getAttribute(WebSecurityConfig.SESSION_KEY);
        int result = userService.modifyPassword(id,password1, password2);
        if(result == -1){
            model.addAttribute("reason","新密码与确认密码不符合");
            return "error";
        }else if(result == 0){
            model.addAttribute("reason","更改密码成功");
            return "success";
        }else{
            model.addAttribute("reason","更改密码失败");
            return "error";
        }

    }

}
