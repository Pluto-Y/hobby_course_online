package com.zsuduan.hobbycourse.service;
import com.zsuduan.hobbycourse.bean.UserBean;

import java.util.List;

public interface UserService {
    //登录
    UserBean loginIn(String account, String password);

    //修改密码
    int modifyPassword(int id, String password1, String password2);

    //显示用户信息
    UserBean getUserInfo(int id);

    //更改用户信息
    int modifyUserInfo(int id,String name,String gender,String tel,String address);

}
