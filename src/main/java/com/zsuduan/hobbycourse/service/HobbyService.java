package com.zsuduan.hobbycourse.service;

import com.zsuduan.hobbycourse.bean.HobbyBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HobbyService {
    int addHobby(int uid,String name,String summary,String others);

    List<HobbyBean> getUserHobby(int uid);

    int modifyUserHobby(int id,String name1,String summary1,String others1);

    HobbyBean getUserHobbyById(@Param("id")int id);

    int deleteUserHobby(int id);
}
