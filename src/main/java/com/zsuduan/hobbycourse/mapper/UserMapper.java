package com.zsuduan.hobbycourse.mapper;

import com.zsuduan.hobbycourse.bean.UserBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface UserMapper {

    //用户登录
    UserBean getInfo(@Param("account") String account, @Param("password") String password);

    int modifyPassword(@Param("id") int id, @Param("password1") String password1);

    UserBean getUserInfo(@Param("id") int id);

    int modifyUserInfo(@Param("id") int id,@Param("name")String name,@Param("gender")String gender,@Param("tel")String tel,@Param("address")String address);


}
