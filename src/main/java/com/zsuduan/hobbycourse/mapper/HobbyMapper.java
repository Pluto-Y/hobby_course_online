package com.zsuduan.hobbycourse.mapper;

import com.zsuduan.hobbycourse.bean.HobbyBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HobbyMapper {
    //添加个人兴趣
    int addHobby(@Param("uid")int uid,@Param("name")String name,@Param("summary")String summary,@Param("others")String others);

    //显示个人兴趣
    List<HobbyBean> getUserHobby(@Param("uid") int uid);

    //修改个人兴趣
    int modifyUserHobby(@Param("id")int id,@Param("name")String name,@Param("summary")String summary,@Param("others")String others);

    //根据id查询个人兴趣
    HobbyBean getUserHobbyById(@Param("id")int id);

    //删除个人兴趣
    int deleteUserHobby(@Param("id")int id);
}
