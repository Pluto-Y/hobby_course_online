package com.zsuduan.hobbycourse.serviceImpl;

import com.zsuduan.hobbycourse.bean.UserBean;
import com.zsuduan.hobbycourse.mapper.UserMapper;
import com.zsuduan.hobbycourse.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    //将DAO（mapper）注入Service
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private UserMapper userMapper;

    //登录
    @Override
    public UserBean loginIn(String account, String password) {

        return userMapper.getInfo(account, password);
    }

    //修改密码
    @Override
    public int modifyPassword(int id, String password1, String password2) {
        if(!password1.equals(password2)){
            return -1;
        }

        int result=userMapper.modifyPassword(id,password1);
        if(result > 0){
            return 0;
        }else{
            return -2;
        }
    }

    //显示用户信息
    @Override
    public UserBean getUserInfo(int id ) {
        return userMapper.getUserInfo(id);
    }

    //修改用户信息
    public int modifyUserInfo(int id,String name,String gender,String tel,String address){
        int result= userMapper.modifyUserInfo(id,name,gender,tel,address);
        if (result>0){
            return 0;
        }else{
            return -1;
        }
    }


}
