package com.zsuduan.hobbycourse.serviceImpl;

import com.zsuduan.hobbycourse.bean.HobbyBean;
import com.zsuduan.hobbycourse.mapper.HobbyMapper;
import com.zsuduan.hobbycourse.service.HobbyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HobbyServiceImpl implements HobbyService {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private HobbyMapper hobbyMapper;

    @Override
    public int addHobby(int uid, String name, String summary, String others) {
        int result= hobbyMapper.addHobby(uid,name,summary,others);
        if (result>0){
            return 0;
        }else {
            return -1;
        }
    }

    @Override
    public List<HobbyBean> getUserHobby(int uid) {
        List<HobbyBean> hobbyBeanList=hobbyMapper.getUserHobby(uid);
        return hobbyBeanList;
    }

    //修改个人兴趣
    @Override
    public int modifyUserHobby(int id, String name1, String summary1, String others1) {
        int result=hobbyMapper.modifyUserHobby(id,name1,summary1,others1);
        if (result>0){
            return 0;
        }else {
            return -1;
        }
    }


    //通过id查询个人兴趣
    @Override
    public HobbyBean getUserHobbyById(int id) {
        return hobbyMapper.getUserHobbyById(id);
    }

    //删除个人兴趣
    @Override
    public int deleteUserHobby(int id) {
        int result=hobbyMapper.deleteUserHobby(id);
        if (result>0){
            return 0;
        }else {
            return -1;
        }
    }
}
