package com.zsuduan.hobbycourse;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.zsuduan.hobbycourse.mapper")
public class HobbyCourseApplication {

    public static void main(String[] args) {

        SpringApplication.run(HobbyCourseApplication.class, args);
    }
}